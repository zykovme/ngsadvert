//
//  AdvertsPresenter.m
//  ngstest
//
//  Created by Zykov Mikhail on 02.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import "AdvertsPresenter.h"
#import "Paginator.h"

@interface AdvertsPresenter ()

@property (strong, nonatomic) Paginator *paginator;
@property (strong, nonatomic) NSMutableArray *items;

@end

@implementation AdvertsPresenter

- (instancetype)init {
    self = [super init];
    if (self) {
        self.paginator = [Paginator new];
    }
    return self;
}

- (void)reload {
    self.paginator.offset = 0;
    self.items = [NSMutableArray array];
    [self nextLoad];
}

- (void)nextLoad {
    
    self.paginator.offset = self.items.count;
    [self.advertsService loadAdvertsWithPaginator:self.paginator success:^(id data) {
        if ([self.delegate respondsToSelector:@selector(reloadData)]) {
            NSArray *result = data;
            [self.items addObjectsFromArray:result];
            [self.delegate reloadData];
        }
    } failure:^(NSError *error) {
        if ([self.delegate respondsToSelector:@selector(showError:)]) {
            [self.delegate showError:error];
        }
    }];
}

@end
