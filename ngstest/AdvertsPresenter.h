//
//  AdvertsPresenter.h
//  ngstest
//
//  Created by Zykov Mikhail on 02.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AdvertsService.h"

@protocol AdvertsPresenterDelegate <NSObject>

- (void)reloadData;
- (void)showError:(NSError *)error;

@end

@interface AdvertsPresenter : NSObject

@property (strong, nonatomic, readonly) NSMutableArray *items;

@property (strong, nonatomic) AdvertsService *advertsService;
@property (weak, nonatomic) id<AdvertsPresenterDelegate> delegate;

- (void)reload;
- (void)nextLoad;

@end
