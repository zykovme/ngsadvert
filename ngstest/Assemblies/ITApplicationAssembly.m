//
//  ITApplicationAssembly.m
//  ngstest
//
//  Created by Zykov Mikhail on 02.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import "ITApplicationAssembly.h"

#import "RequestService.h"
#import "AdvertsService.h"
#import "AdvertsPresenter.h"
#import "AdvertsViewController.h"
#import "ShareAdvertService.h"


@implementation ITApplicationAssembly


- (RequestService *)requestService {
    return [TyphoonDefinition withClass:[RequestService class] configuration:^(TyphoonDefinition *definition) {
        [definition useInitializer:@selector(initWithBaseURL:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:[NSURL URLWithString:@"http://do.ngs.ru/api/v1"]];
        }];
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (AdvertsService *)advertsService {
    return [TyphoonDefinition withClass:[AdvertsService class] configuration:^(TyphoonDefinition *definition) {
        [definition injectProperty:@selector(requestService) with:[self requestService]];
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (AdvertsPresenter *)advertsPresenter {
    return [TyphoonDefinition withClass:[AdvertsPresenter class] configuration:^(TyphoonDefinition *definition) {
        [definition injectProperty:@selector(advertsService) with:[self advertsService]];
        [definition injectProperty:@selector(delegate) with:[self advertsViewController]];
    }];
}

- (AdvertsViewController *)advertsViewController {
    return [TyphoonDefinition withClass:[AdvertsViewController class] configuration:^(TyphoonDefinition *definition) {
        [definition injectProperty:@selector(advertsPresenter) with:[self advertsPresenter]];
        [definition injectProperty:@selector(shareAdvertService) with:[self shareAdvertService]];
    }];
}

- (ShareAdvertService *)shareAdvertService {
    return [TyphoonDefinition withClass:[ShareAdvertService class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
    }];
}


@end
