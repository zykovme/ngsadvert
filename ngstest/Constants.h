//
//  Constants.h
//  ngstest
//
//  Created by Zykov Mikhail on 02.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PERFORM_BLOCK_IF_NOT_NIL(block, ...) \
if (block) {\
block(__VA_ARGS__); \
}

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define is_iOS7 SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")
#define is_iOS8 SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")
#define is_iOS9 SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")

@interface Constants : NSObject

@end
