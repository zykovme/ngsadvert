//
//  AdvertCell.h
//  ngstest
//
//  Created by Zykov Mikhail on 02.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Advert;

@interface AdvertCell : UITableViewCell

- (void)fillByAdvert:(Advert *)advert;

@end
