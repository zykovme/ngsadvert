//
//  AdvertCell.m
//  ngstest
//
//  Created by Zykov Mikhail on 02.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import "AdvertCell.h"
#import "Advert.h"
// Utils
#import "NSString+CostFormat.h"
#import "NSDateFormatter+SharedInstance.h"
// Library
#import <SDWebImage/UIImageView+WebCache.h>

@interface AdvertCell ()

@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end

@implementation AdvertCell

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
    
    self.dateLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.dateLabel.frame);
    self.titleLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.titleLabel.frame);
    self.priceLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.priceLabel.frame);
}

- (void)fillByAdvert:(Advert *)advert {
    
    NSString *stringDate = [NSString stringForDate:advert.updateDate];
    NSString *tag = advert.tags.firstObject ?: @"";
    NSString *apersand = stringDate.length > 0 && tag.length > 0 ? @" · " : @"";
    self.dateLabel.text = [@[[tag capitalizedString], apersand, stringDate] componentsJoinedByString:@""];
    self.titleLabel.text = advert.title;
    self.priceLabel.text = [NSString stringForCost:advert.cost];
    [self.coverImageView sd_setImageWithURL:advert.coverUrl placeholderImage:[UIImage imageNamed:@"no_thumb"] options:SDWebImageRetryFailed];
}

@end
