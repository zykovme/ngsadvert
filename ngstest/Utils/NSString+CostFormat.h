//
//  NSString+CostFormat.h
//  ngstest
//
//  Created by Zykov Mikhail on 03.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString (CostFormat)

+ (NSString *)stringForCost:(NSNumber *)cost;
+ (NSString *)stringForDate:(NSDate *)date;

@end
