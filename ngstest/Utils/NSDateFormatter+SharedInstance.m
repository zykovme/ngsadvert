//
//  NSDateFormatter+SharedInstance.m
//  ngstest
//
//  Created by Zykov Mikhail on 03.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import "NSDateFormatter+SharedInstance.h"

@implementation NSDateFormatter (SharedInstance)

+ (instancetype)sharedFormatter {
    static dispatch_once_t onceToken;
    static NSDateFormatter *shared = nil;
    dispatch_once(&onceToken, ^{
        shared = [[[self class] alloc] init];
    });
    return shared;
}

+ (instancetype)sharedFormatterWithDateFormate {
    NSDateFormatter *dateFormater = [self sharedFormatter];
    [dateFormater setDateFormat:@"dd.MM.yyyy"];
    return dateFormater;
}

// 2016-07-03T15:12:08+0600
+ (instancetype)sharedFormatterForServer {
    NSDateFormatter *dateFormater = [self sharedFormatter];
    [dateFormater setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
    return dateFormater;
}

@end
