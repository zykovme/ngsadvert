//
//  NSNumberFormatter+ShareInstance.h
//  ngstest
//
//  Created by Zykov Mikhail on 03.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumberFormatter (SharedInstance)

+ (instancetype)sharedFormatter;
+ (instancetype)sharedFormatterWithDecimalStyle;

@end
