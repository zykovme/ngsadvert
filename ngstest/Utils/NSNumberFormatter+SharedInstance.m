//
//  NSNumberFormatter+ShareInstance.m
//  ngstest
//
//  Created by Zykov Mikhail on 03.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import "NSNumberFormatter+SharedInstance.h"

@implementation NSNumberFormatter (SharedInstance)

+ (instancetype)sharedFormatter {
    static dispatch_once_t onceToken;
    static NSNumberFormatter *shared = nil;
    dispatch_once(&onceToken, ^{
        shared = [[[self class] alloc] init];
        [shared setNumberStyle:NSNumberFormatterDecimalStyle];
    });
    return shared;
}

+ (instancetype)sharedFormatterWithDecimalStyle {
    NSNumberFormatter *numberFormater = [NSNumberFormatter sharedFormatter];
    [numberFormater setNumberStyle:NSNumberFormatterDecimalStyle];
    return numberFormater;
}

@end
