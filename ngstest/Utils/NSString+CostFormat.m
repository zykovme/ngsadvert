//
//  NSString+CostFormat.m
//  ngstest
//
//  Created by Zykov Mikhail on 03.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import "NSString+CostFormat.h"
#import "NSNumberFormatter+SharedInstance.h"
#import "NSDateFormatter+SharedInstance.h"

@implementation NSString (CostFormat)

+ (NSString *)stringForCost:(NSNumber *)cost {
    if (cost == nil || ![cost isKindOfClass:[NSNumber class]] || [cost isEqualToNumber:@0]) {
        return @"цена не указана";
    } else {
        NSString *someString = [[NSNumberFormatter sharedFormatterWithDecimalStyle] stringFromNumber:cost];
        return [NSString stringWithFormat:@"%@ руб.", someString];
    }
}

+ (NSString *)stringForDate:(NSDate *)date {
    if (date == nil || ![date isKindOfClass:[NSDate class]]) {
        return @"";
    } else {
        return [[NSDateFormatter sharedFormatterWithDateFormate] stringFromDate:date];
    }
}

@end
