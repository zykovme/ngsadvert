//
//  RequestService.h
//  ngstest
//
//  Created by Zykov Mikhail on 02.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^Success)(id _Nullable data);
typedef void(^Failure)(NSError * _Nullable error);
typedef void(^Progress)(NSProgress * _Nullable progress);

typedef void (^AFHTTPRequestSuccess)(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject);
typedef void (^AFHTTPRequestFailure)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error);
typedef void (^AFHTTPRequestProgress)(NSProgress * _Nonnull downloadProgress);



typedef NS_ENUM(NSUInteger, REQUEST_METHOD) {
    REQUEST_METHOD_GET = 0,
    REQUEST_METHOD_POST,
    REQUEST_METHOD_PUT,
    REQUEST_METHOD_PATCH,
    REQUEST_METHOD_DELETE,
    REQUEST_METHOD_HEAD
};

@interface RequestService : NSObject

- (nonnull instancetype)initWithBaseURL:(nonnull NSURL *)url NS_DESIGNATED_INITIALIZER;

- (nonnull NSURLSessionDataTask *)GET:(nonnull NSString *)URLString parameters:(nullable id)parameters progress:(nullable AFHTTPRequestProgress)progress success:(nullable Success)success failure:(nullable Failure)failure;

@end
