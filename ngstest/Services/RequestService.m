//
//  RequestService.m
//  ngstest
//
//  Created by Zykov Mikhail on 02.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import "RequestService.h"
#import <AFNetworking/AFNetworking.h>

@interface RequestService ()

@property (strong, nonatomic) AFHTTPSessionManager *manager;

@end

@implementation RequestService

- (instancetype)init {
    self = [self initWithBaseURL:[NSURL new]];
    return self;
}

- (instancetype)initWithBaseURL:(NSURL *)url /*errorHandler:(RequestErrorHandlerType)errorHandler*/ {
    self = [super init];
    if (self) {
        self.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
        self.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        self.manager.responseSerializer = [AFJSONResponseSerializer serializer];
    }
    return self;
}

- (NSURLSessionDataTask *)GET:(NSString *)URLString parameters:(id)parameters progress:(AFHTTPRequestProgress)progress success:(Success)success failure:(Failure)failure {
    return [self.manager GET:URLString
                  parameters:parameters
                    progress:progress
                     success:[self mapSuccess:success failure:failure]
                     failure:[self mapFailure:failure]];
}

- (AFHTTPRequestSuccess)mapSuccess:(Success)success failure:(Failure)failure {
    return ^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        PERFORM_BLOCK_IF_NOT_NIL(success, responseObject);
    };
}

- (AFHTTPRequestFailure)mapFailure:(Failure)failure {
    return ^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        PERFORM_BLOCK_IF_NOT_NIL(failure, error);
    };
}


@end
