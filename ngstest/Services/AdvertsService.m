//
//  AdvertsService.m
//  ngstest
//
//  Created by Zykov Mikhail on 30.06.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import "AdvertsService.h"
#import "Paginator.h"
#import "Advert.h"

#import <AFNetworking/AFNetworking.h>
#import <EasyMapping/EasyMapping.h>

NSString * kAdvertsUrlFormat = @"adverts/?include=uploads,tags&fields=short_images,cost,update_date&limit=%ld&offset=%ld";
NSString * kAdvertsUrlWithPaginator(Paginator *paginator) {
    return [NSString stringWithFormat:kAdvertsUrlFormat, (long)paginator.limit, (long)paginator.offset];
}

@interface AdvertsService ()

@end

@implementation AdvertsService

- (void)loadAdvertsWithPaginator:(Paginator *)paginator success:(Success)success failure:(Failure)failure {
    [self.requestService GET:kAdvertsUrlWithPaginator(paginator) parameters:nil progress:nil success:^(NSDictionary *result) {
        
        NSDictionary *uploads = result[@"linked"][@"uploads"];
        NSDictionary *tags = result[@"linked"][@"tags"];
        
        NSArray *adverts = [EKMapper arrayOfObjectsFromExternalRepresentation:result[@"adverts"] withMapping:[Advert objectMapping]];
        for (Advert *advert in adverts) {
            NSDictionary *upload = uploads[advert.coverId];
            if (upload != nil) {
                NSString *host = upload[@"domain"];
                NSString *path = [NSString stringWithFormat:@"%@.%@", upload[@"file_name"], upload[@"file_extension"]];
                if (![path hasPrefix:@"/"]) {
                    path = [@"/" stringByAppendingString:path];
                }
                advert.coverUrl = [[NSURL alloc] initWithScheme:@"http" host:host path:path];
            }
            
            NSMutableArray *t = [NSMutableArray array];
            for (NSNumber *tagID in advert.tagIds) {
                if (tags[tagID][@"title"] != nil) {
                    [t addObject:tags[tagID][@"title"]];
                }
            }
            advert.tags = t;
        }
        PERFORM_BLOCK_IF_NOT_NIL(success, adverts);
    } failure:failure];
}

@end
