//
//  ShareAdvertService.m
//  ngstest
//
//  Created by Zykov Mikhail on 04.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import "ShareAdvertService.h"

#import "Advert.h"
//Library
#import <SDWebImage/SDWebImageManager.h>

@implementation ShareAdvertService

#pragma mark - Public

- (void)presentShareControllerWithAdvert:(Advert *)advert onViewcontroller:(UIViewController *)viewController {
    NSString *text = advert.title;
    NSURL *url = advert.coverUrl;
    
    if (url != nil) {
        SDWebImageManager *mng = [SDWebImageManager sharedManager];
        [mng downloadImageWithURL:url
                          options:0
                         progress:nil
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            [self showShareControllerImage:image message:text url:nil onViewcontroller:viewController];
                        }];
    } else {
        [self showShareControllerImage:nil message:text url:nil onViewcontroller:viewController];
    }
}


#pragma mark - Private


- (void)showShareControllerImage:(UIImage *)image message:(NSString *)messege url:(NSURL *)url onViewcontroller:(UIViewController *)viewController {
    
    NSMutableArray *items = [NSMutableArray array];
    if (image) {
        [items addObject:image];
    }
    if (messege) {
        [items addObject:messege];
    }
    if (url) {
        [items addObject:url];
    }
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:items
                                                                                         applicationActivities:nil];
    
    if (is_iOS9) {
        activityViewController.excludedActivityTypes = @[UIActivityTypeOpenInIBooks, UIActivityTypeAirDrop, UIActivityTypeAssignToContact];
    } else {
        activityViewController.excludedActivityTypes = @[UIActivityTypeAirDrop, UIActivityTypeAssignToContact];
    }
    
    [viewController presentViewController:activityViewController animated:YES completion:nil];
}


@end
