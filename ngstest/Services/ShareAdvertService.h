//
//  ShareAdvertService.h
//  ngstest
//
//  Created by Zykov Mikhail on 04.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Advert;

@interface ShareAdvertService : NSObject

- (void)presentShareControllerWithAdvert:(Advert *)advert onViewcontroller:(UIViewController *)viewController;

@end
