//
//  AdvertsService.h
//  ngstest
//
//  Created by Zykov Mikhail on 30.06.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RequestService.h"

@class Paginator;

@interface AdvertsService : NSObject

@property (strong, nonatomic) RequestService *requestService;

- (void)loadAdvertsWithPaginator:(Paginator *)paginator success:(Success)success failure:(Failure)failure;

@end
