//
//  AppDelegate.h
//  ngstest
//
//  Created by Zykov Mikhail on 29.06.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

