//
//  AdvertsViewController.h
//  ngstest
//
//  Created by Zykov Mikhail on 02.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AdvertsPresenter, ShareAdvertService;

@interface AdvertsViewController : UIViewController

@property (strong, nonatomic) AdvertsPresenter *advertsPresenter;
@property (strong, nonatomic) ShareAdvertService *shareAdvertService;

@end
