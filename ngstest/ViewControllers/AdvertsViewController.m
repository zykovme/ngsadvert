//
//  AdvertsViewController.m
//  ngstest
//
//  Created by Zykov Mikhail on 02.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import "AdvertsViewController.h"
#import "AdvertCell.h"
#import "AdvertsPresenter.h"
#import "ShareAdvertService.h"
//Library
#import <UIScrollView+InfiniteScroll.h>


@interface AdvertsViewController () <UITableViewDelegate, UITableViewDataSource, AdvertsPresenterDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;
@end


@implementation AdvertsViewController


- (void)viewDidLoad {
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshTable:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicatorView.hidesWhenStopped = YES;
    [self.activityIndicatorView startAnimating];
    self.tableView.backgroundView = self.activityIndicatorView;
    
    [self.advertsPresenter reload];
    [self.tableView addInfiniteScrollWithHandler:^(UICollectionView* collectionView) {
        [self.advertsPresenter nextLoad];
    }];
}


#pragma mark - <AdvertsPresenterDelegate>


- (void)reloadData {
    [self.activityIndicatorView stopAnimating];
    [self.refreshControl endRefreshing];
    [self.tableView finishInfiniteScroll];
    self.tableView.estimatedRowHeight = 130.0;
    [self.tableView reloadData];
}

- (void)showError:(NSError *)error {

}


#pragma mark - <UITableViewDataSource>


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.advertsPresenter.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AdvertCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AdvertCell class]) forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    return cell;
}


#pragma mark - <UITableViewDelegate>


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    id item = self.advertsPresenter.items[indexPath.row];
    [self.shareAdvertService presentShareControllerWithAdvert:item onViewcontroller:self];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.alpha = 0.4;
    [UIView animateWithDuration:0.3 animations:^{
        cell.alpha = 1;
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    static AdvertCell *cell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AdvertCell class])];
    });
    [self configureCell:cell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:cell];
}


#pragma mark - Private


- (void)refreshTable:(UIRefreshControl *)refreshControl {
    [self.advertsPresenter reload];
}

- (void)configureCell:(AdvertCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row > 0 && indexPath.row < self.advertsPresenter.items.count) {
        id item = self.advertsPresenter.items[indexPath.row];
        [cell fillByAdvert:item];
    }
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)cell {
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    cell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tableView.bounds), CGRectGetHeight(cell.bounds));
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    CGFloat height = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    height += 1;
    return height;
}


@end
