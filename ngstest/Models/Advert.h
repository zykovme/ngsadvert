//
//  Advert.h
//  ngstest
//
//  Created by Zykov Mikhail on 02.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EasyMapping/EasyMapping.h>

@interface Advert : NSObject <EKMappingProtocol>

@property (strong, nonatomic) NSNumber *uid;
@property (strong, nonatomic) NSURL    *coverUrl;
@property (strong, nonatomic) NSNumber *coverId;

@property (strong, nonatomic) NSDate   *updateDate;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSNumber *cost;

@property (strong, nonatomic) NSArray<NSString*> *tags;
@property (strong, nonatomic) NSArray<NSNumber*> *tagIds;

@end
