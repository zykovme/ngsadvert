//
//  Paginator.m
//  ngstest
//
//  Created by Zykov Mikhail on 02.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import "Paginator.h"

@implementation Paginator

- (instancetype)init {
    self = [super init];
    if (self) {
        self.offset = 0;
        self.limit = 40;
    }
    return self;
}

@end
