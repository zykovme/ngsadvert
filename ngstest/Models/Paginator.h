//
//  Paginator.h
//  ngstest
//
//  Created by Zykov Mikhail on 02.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Paginator : NSObject

@property (assign, nonatomic) NSInteger offset;
@property (assign, nonatomic) NSInteger limit;

@end
