//
//  Advert.m
//  ngstest
//
//  Created by Zykov Mikhail on 02.07.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import "Advert.h"
#import "NSDateFormatter+SharedInstance.h"

@implementation Advert

+ (EKObjectMapping *)objectMapping {
    return [EKObjectMapping mappingForClass:[self class] withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath: @"update_date" toProperty:@"updateDate" withValueBlock:^id(NSString *key, id value) {
            if (value != nil && ![value isKindOfClass:[NSNull class]]) {
                NSDateFormatter *dateFormater = [NSDateFormatter sharedFormatterForServer];
                return [dateFormater dateFromString:value];
            }
            return nil;
        }];
        
        [mapping mapPropertiesFromDictionary:@{
                                               @"id"    : @"uid",
                                               @"title" : @"title",
                                               @"cost"  : @"cost",
                                               @"short_images.main.links.thumb" : @"coverId",
                                               @"links.tags" : @"tagIds"
                                                 }];
    }];
}

@end
