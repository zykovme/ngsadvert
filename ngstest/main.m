//
//  main.m
//  ngstest
//
//  Created by Zykov Mikhail on 29.06.16.
//  Copyright © 2016 Zykov Mikhail. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
